describe('Screenshot Test', () => {
    it('Checks if take screenshot works ', () => {
        cy.takeScreenshot('https://google.com');
        cy
            .get('.screenshot', {timeout: 20000})
            .should('have.attr', 'src')
            .then(src => {
               expect(src.length)
                   .to.be.eq(68718);
            });
        cy
            .get('.screenshot-resolution-name')
            .should('have.text', '1080p Desktop')
    })
});
